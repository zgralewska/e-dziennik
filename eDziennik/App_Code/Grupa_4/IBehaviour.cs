﻿using eDziennik.App_SchoolDb;
using System.Linq;

namespace eDziennik.App_Code.Grupa_4
{
    public interface IBehaviour
    {
        bool AddDocument(Document document);
        IQueryable<Document> GetAllDocuments();
        Document GetEmptyDocument();
        bool UpdateDocument(Document document);
        Document GetDocumentByDocumentId(int DocumentId);
        IQueryable<Document> GetDocumentsByStudentId(int StudentId);
        IQueryable<Class> GetAllClasses();
        Class GetClass(int ClassId);
        IQueryable<Student> GetAllStudentsFromClass(int ClassId);
        Student GetStudent(int StudentId);

        Teacher GetTeacher(int TeacherId);

    }
}
