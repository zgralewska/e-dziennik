﻿using eDziennik.App_SchoolDb;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace eDziennik.App_Code.Grupa_4
{
    public static class TestHelpers
    {
        public static void AddSchoolToDB(School school)
        {
            using (var db = new SchoolDb())
            {
                db.Schools.Add(school);
                db.SaveChanges();
            }
        }

        public static void RemoveSchool(string schoolName)
        {
            using (var db = new SchoolDb())
            {
                foreach (var school in db.Schools.Where(s => s.SchoolName == schoolName))
                {
                    db.Schools.Remove(school);
                    db.Entry(school).State = EntityState.Deleted;
                }

                db.SaveChanges();
            }
        }

        public static void RemoveTeacher(string teacherName)
        {
            using (SchoolDb db = new SchoolDb())
            {
                var teacher = db.Teachers.Where(t => t.TeacherFirstName == teacherName);
                db.Teachers.RemoveRange(teacher);

                db.SaveChanges();
            }
        }

        public static void RemoveClass(string classSymbol)
        {
            using (SchoolDb db = new SchoolDb())
            {
                var _class = db.Classes.Where(c => c.ClassSymbol == classSymbol);
                db.Classes.RemoveRange(_class);

                db.SaveChanges();
            }
        }

        public static void RemoveStudent(string studentName)
        {
            using (SchoolDb db = new SchoolDb())
            {
                var student = db.Students.Where(c => c.StudentFirstName == studentName);
                db.Students.RemoveRange(student);

                db.SaveChanges();
            }
        }

        public static void RemoveDocument(string documentContent)
        {
            using (SchoolDb db = new SchoolDb())
            {
                var document = db.Documents.Where(d => d.DocumentContent == documentContent);
                db.Documents.RemoveRange(document);

                db.SaveChanges();
            }
        }

        public static int CreateTeacher(string teacherName)
        {
            using (SchoolDb db = new SchoolDb())
            {
                var teacher = new Teacher { TeacherFirstName = teacherName };
                db.Teachers.Add(teacher);
                db.SaveChanges();

                return db.Teachers.Where(t => t.TeacherFirstName == teacherName).First().TeacherId;
            }
        }

        public static int CreateClass(string classSymbol)
        {
            using (var db = new SchoolDb())
            {
                db.Classes.Add(new Class { ClassSymbol = classSymbol });
                db.SaveChanges();

                return db.Classes.Where(c => c.ClassSymbol == classSymbol).First().ClassId;
            }
        }
        
        public static int CreateStudent(string studentName, int classId)
        {
            using (var db = new SchoolDb())
            {
                db.Students.Add(new Student { StudentFirstName = studentName, ClassId = classId });
                db.SaveChanges();

                return db.Students.Where(s => s.StudentFirstName == studentName).First().StudentId;
            }
        }

        public static int CreateDocument(string documentContent, int studentId, int teacherId)
        {
            using (var db = new SchoolDb())
            {
                db.Documents.Add(new Document { DocumentContent = documentContent, DocumentDate = DateTime.Now, StudentId = studentId, TeacherId = teacherId });
                db.SaveChanges();

                return db.Documents.Where(d => d.DocumentContent == documentContent).First().DocumentId;
            }
        }

        public static int GetSchoolId(string schoolName)
        {
            using (var db = new SchoolDb())
            {
                var schools = db.Schools.Where(s => s.SchoolName == schoolName);

                // wiem, że ogólnie taka sytuacja może sie zdarzyć - 
                //      nigdzie w specyfikacji nie ma nic o walidacji nazwy szkoły == mogą być dwie o tej samej nazwie
                // ponieważ jest to helperek na potrzeby mojego testu to nazwa szkoły musi być unikalna
                if (schools.Count() != 1)
                    throw new InvalidOperationException("There was none or more than one school with this name.");

                return schools.First().SchoolId;
            }
        }

        public static List<Document> GetAllDocuments()
        {
            using (var db = new SchoolDb())
            {
                var result = db.Documents.AsQueryable();
                return db.Documents.ToList();
            }
        }
    }
}