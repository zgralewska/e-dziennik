﻿using eDziennik.App_SchoolDb;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace eDziennik.App_Code.Grupa_4
{
    public class Behaviour : IBehaviour
    {
        private readonly SchoolDb _db;

        public Behaviour()
        {
            _db = new SchoolDb();
        }

        public bool AddDocument(Document document)
        {
            try
            {
                _db.Documents.Add(document);
                _db.SaveChanges();
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        public IQueryable<Document> GetAllDocuments()
        {
            return _db.Documents;
        }

        public App_SchoolDb.Document GetEmptyDocument()
        {
            return new Document();
        }

        public bool UpdateDocument(Document document)
        {
            try
            {
                _db.Documents.Attach(document);
                _db.Entry(document).State = System.Data.Entity.EntityState.Modified;
                _db.SaveChanges();
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        public Document GetDocumentByDocumentId(int DocumentId)
        {
            return _db.Documents.Where(d => d.DocumentId == DocumentId).SingleOrDefault();
        }

        public IQueryable<Document> GetDocumentsByStudentId(int Studentid)
        {
            return _db.Documents.Where(d => d.StudentId == Studentid);
        }

        public IQueryable<Class> GetAllClasses()
        {
            return _db.Classes;
        }

        public Class GetClass(int ClassId)
        {
            return _db.Classes.Where(c => c.ClassId == ClassId).SingleOrDefault();
        }

        public IQueryable<Student> GetAllStudentsFromClass(int ClassId)
        {
            return _db.Students.Where(s => s.ClassId == ClassId);
        }

        public Student GetStudent(int StudentId)
        {
            return _db.Students.Where(s => s.StudentId == StudentId).SingleOrDefault();
        }

        public Teacher GetTeacher(int TeacherId)
        {
            return _db.Teachers.Where(t => t.TeacherId == TeacherId).SingleOrDefault();
        }

    }

    public static class eDziennikConstans
    {
        public const string TeacherName = "TestTeacherName";
        public const string ClassSymbol = "ABC";
        public const string StudentName = "TestStudentName";
        public const string StudentName2 = "TestStudentName_2";
        public const string DocumentContent = "TestDocumentContent";
        public const string DocumentContent2 = "TestDocumentContent_2";
        public const string DocumentContent3 = "TestDocumentContent_3";
    }

    [TestClass]
    public class BehaviourTests
    {
        public Behaviour Target { get; private set; }

        public BehaviourTests()
        {
            Target = new Behaviour();
        }

        [TestInitialize]
        public void CleaningBeforeEachTest()
        {
            TestHelpers.RemoveStudent(eDziennikConstans.StudentName);
            TestHelpers.RemoveTeacher(eDziennikConstans.TeacherName);
            TestHelpers.RemoveClass(eDziennikConstans.ClassSymbol);
            TestHelpers.RemoveDocument(eDziennikConstans.DocumentContent);
            TestHelpers.RemoveDocument(eDziennikConstans.DocumentContent2);
            TestHelpers.RemoveDocument(eDziennikConstans.DocumentContent3);
        }

        [TestClass]
        public class DocumentReletedTests : BehaviourTests
        {
            private int TeacherId;
            private int ClassId;
            private int StudentId;

            [TestInitialize]
            public void BeforeEachTest()
            {
                TeacherId = TestHelpers.CreateTeacher(eDziennikConstans.TeacherName);
                ClassId = TestHelpers.CreateClass(eDziennikConstans.ClassSymbol);
                StudentId = TestHelpers.CreateStudent(eDziennikConstans.StudentName, ClassId);
                var documentId = TestHelpers.CreateDocument(eDziennikConstans.DocumentContent, StudentId, TeacherId);
            }

            [TestMethod]
            public void GetAllDocuments_ReturnsAtLeastOneDocument()
            {
                Assert.IsTrue(Target.GetAllDocuments().Any(doc => doc.DocumentContent == eDziennikConstans.DocumentContent));
            }

            [TestMethod]
            public void GetEmptyDocument_ReturnsObjectOfTypeDocument()
            {
                var result = Target.GetEmptyDocument();
                Assert.IsTrue(result.GetType() == typeof(Document));
                Assert.AreEqual(null, result.DocumentContent);
                Assert.AreEqual(default(DateTime), result.DocumentDate);
                Assert.AreEqual(0, result.DocumentId);
                Assert.AreEqual(default(DocumentType), result.DocumentType);
                Assert.AreEqual(null, result.Student);
                Assert.AreEqual(0, result.StudentId);
                Assert.AreEqual(null, result.Teacher);
                Assert.AreEqual(0, result.TeacherId);
            }

            [TestMethod]
            public void AddDocument_AddsDocumentToDatabase()
            {
                var result = Target.AddDocument(new Document
                {
                    DocumentContent = eDziennikConstans.DocumentContent2,
                    DocumentDate = DateTime.Now,
                    TeacherId = TeacherId,
                    StudentId = StudentId
                });

                Assert.IsTrue(result);
                Assert.IsTrue(TestHelpers.GetAllDocuments().Any(d => d.DocumentContent == eDziennikConstans.DocumentContent2));
            }

            [TestMethod]
            public void AddDocument_WithNullObject_ReturnsFalse()
            {
                Assert.IsFalse(Target.AddDocument(null));
            }

            [TestMethod]
            public void AddDocument_WithInvalidData_ReturnsFalse()
            {
                Assert.IsFalse(Target.AddDocument(new Document { DocumentDate = new DateTime(1, 1, 1, 1, 1, 1) }));
            }

            [TestMethod]
            public void UpdateDocument_UpdatesGivenDocument()
            {
                // Arrange
                var documentId = TestHelpers.CreateDocument(eDziennikConstans.DocumentContent2, StudentId, TeacherId);

                var documentToUpdate = TestHelpers.GetAllDocuments().Where(d => d.DocumentId == documentId).Single();
                documentToUpdate.DocumentContent = eDziennikConstans.DocumentContent3;

                // Act
                var result = Target.UpdateDocument(documentToUpdate);

                // Assert
                Assert.IsTrue(result);

                var updated = TestHelpers.GetAllDocuments().Where(d => d.DocumentId == documentId).Single();
                Assert.AreEqual(eDziennikConstans.DocumentContent3, updated.DocumentContent);
            }

            [TestMethod]
            public void UpdateDocument_ReturnsFalseIfAnythingWrong()
            {
                // nie da się tego sprawdzić w żaden ładny sposób, zatem podam Entity datę, której nie obsłuży

                // Arrange
                var newDate = new DateTime(1, 1, 1, 1, 1, 1);
                var documentId = TestHelpers.CreateDocument(eDziennikConstans.DocumentContent2, StudentId, TeacherId);

                var documentToUpdate = TestHelpers.GetAllDocuments().Where(d => d.DocumentId == documentId).Single();
                documentToUpdate.DocumentDate = newDate;

                // Act
                var result = Target.UpdateDocument(documentToUpdate);

                // Assert
                Assert.IsFalse(result);

                var updated = TestHelpers.GetAllDocuments().Where(d => d.DocumentId == documentId).Single();
                Assert.AreNotEqual(newDate, updated.DocumentDate);
            }

            [TestMethod]
            public void GetDocumentById_ReturnsValidDocument()
            {
                var documentId = TestHelpers.CreateDocument(eDziennikConstans.DocumentContent2, StudentId, TeacherId);

                var result = Target.GetDocumentByDocumentId(documentId);
                var expected = TestHelpers.GetAllDocuments().Where(d => d.DocumentId == documentId).Single();

                Assert.AreEqual(expected.DocumentContent, result.DocumentContent);
            }

            [TestMethod]
            public void GetDocumentByStudentId_ReturnsValidStudent()
            {
                var result = Target.GetDocumentsByStudentId(StudentId);
                Assert.IsTrue(result.Any(d => d.DocumentContent == eDziennikConstans.DocumentContent));
            }
        }

        [TestClass]
        public class ClassRelatedTests : BehaviourTests
        {
            private int ClassId;
            private int StudentId;

            [TestInitialize]
            public void BeforeEachTest()
            {
                ClassId = TestHelpers.CreateClass(eDziennikConstans.ClassSymbol);
                StudentId = TestHelpers.CreateStudent(eDziennikConstans.StudentName, ClassId);
            }

            [TestMethod]
            public void GetAllClasses_ReturnsAllClasses()
            {
                Assert.IsTrue(Target.GetAllClasses().Any(c => c.ClassId == ClassId));
            }

            [TestMethod]
            public void GetClass_ReturnsValidObject()
            {
                var result = Target.GetClass(ClassId);
                Assert.AreEqual(eDziennikConstans.ClassSymbol, result.ClassSymbol);
            }

            [TestMethod]
            public void GetClass_WithNotExistingClassId_ReturnsNull()
            {
                Assert.IsNull(Target.GetClass(-1));
            }

            [TestMethod]
            public void GetAllStudentsFromClass_ReturnsStudentsSignedToClass()
            {
                Assert.IsTrue(Target.GetAllStudentsFromClass(ClassId).Any(s => s.StudentId == StudentId));
            }
        }

        [TestClass]
        public class StudentAndTeacherRelatedTests : BehaviourTests
        {
            private int ClassId;
            private int StudentId;
            private int TeacherId;

            [TestInitialize]
            public void BeforeEachTest()
            {
                ClassId = TestHelpers.CreateClass(eDziennikConstans.ClassSymbol);
                StudentId = TestHelpers.CreateStudent(eDziennikConstans.StudentName, ClassId);
                TeacherId = TestHelpers.CreateTeacher(eDziennikConstans.TeacherName);
            }

            [TestMethod]
            public void GetStudent_ReturnsStudent()
            {
                var result = Target.GetStudent(StudentId);
                Assert.AreEqual(eDziennikConstans.StudentName, result.StudentFirstName);
            }

            [TestMethod]
            public void GetTeacher_ReturnsTeacher()
            {
                var result = Target.GetTeacher(TeacherId);
                Assert.AreEqual(eDziennikConstans.TeacherName, result.TeacherFirstName);
            }
        }
    }
}