﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SchoolMasterPage.Master" AutoEventWireup="true" CodeBehind="BehaviourViewTeacher.aspx.cs" Inherits="eDziennik.App_View.Grupa_4.BehaviourViewTeacher" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>Wybierz klasę i ucznia</h2>
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div>
        <asp:DropDownList ID="DdlClasses" runat="server" OnSelectedIndexChanged="DdlClasses_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>
    </div>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div>
                <asp:DropDownList ID="DdlStudents" runat="server" AutoPostBack="True"></asp:DropDownList>
                <asp:Button ID="Button1" runat="server" Text="Wybierz" OnClick="Button1_Click" />
            </div>
            
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="DdlClasses" EventName="SelectedIndexChanged" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
