﻿using eDziennik.App_Code.Grupa_4;
using eDziennik.App_SchoolDb;
using eDziennik.App_View.Grupa_4.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.ModelBinding;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace eDziennik.App_View.Grupa_4
{
    public partial class BehaviourViewDocument : System.Web.UI.Page, IHelper
    {
        private readonly IBehaviour _behaviour;
        private int studentId;
        private ContentPlaceHolder cph;
        public Info Info { get; set; }
        public BehaviourViewDocument()
        {
            this._behaviour = new Behaviour();
        }

        public Student GetStudent([QueryString("ID")] string studentId)
        {
            Student student = null;
            int id = 0;
            int.TryParse(studentId, out id);

            if (id > 0)
                student = _behaviour.GetStudent(id);

            if (student == null || id == 0)
            {
                Info = new Info
                {
                    InfoTitle = "Błędny numer ID ucznia",
                    InfoDescription = "Możliwe, że próbujesz pobrać informacje o uczniu, który nie istnieje w bazie."
                };

                Server.Transfer("SchoolInfo.aspx");
            }

            return student;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
                Student student = null;
                student = GetStudent(Request.QueryString["ID"]);
                studentId = student.StudentId;

                cph = (ContentPlaceHolder)Master.FindControl("ContentPlaceHolder1");
                Label studentName = ((Label)cph.FindControl("Label1"));

                studentName.Text = student.StudentFirstName + " " + student.StudentLastName;

                var documents = _behaviour.GetDocumentsByStudentId(studentId);
                GridView1.DataSource = documents.OrderByDescending(d => d.DocumentDate).ToList();
                GridView1.DataBind();
            
        }

        protected void Breprimand_Click(object sender, EventArgs e)
        {
            string url = "/App_View/Grupa_4/BehaviourViewAddDocument.aspx?";
            url += "ID=" + studentId + "&d=" + 1;
            Response.Redirect(url);
        }

        protected void Bcommendation_Click(object sender, EventArgs e)
        {
            string url = "/App_View/Grupa_4/BehaviourViewAddDocument.aspx?";
            url += "ID=" + studentId + "&d=" + 0;
            Response.Redirect(url);
        }
    }
}